<?php

use BaruschkaBrothers\RestAPI\Config\Config;

// Include bootstrap file
require_once __DIR__ . '/bootstrap.php';

// Return default active app object
return new BaruschkaBrothers\RestAPI\Core\Application(Config::ENVIRONMENT_TYPE, Config::DEBUG_MODE);