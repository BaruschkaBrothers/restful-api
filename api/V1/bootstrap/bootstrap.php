<?php

// Load global configuration
include_once __DIR__ . '/../../global_config.php';

// Loads all composer dependencies
$composer_autoloader = require_once __DIR__ . '/../lib/composer/vendor/autoload.php';

// Register autoloader for custom classes
spl_autoload_register(function ($classname) {
	require_once (__DIR__ . "/../src/" . str_replace("\\", "/", $classname) . ".php");
});

// Setup default time zone
date_default_timezone_set(\BaruschkaBrothers\RestAPI\Config\Config::SERVER_TIMEZONE);

// Register OAuth2 Server Classloader
\OAuth2\Autoloader::register();

// Define Doctrine Annotations Loader
\Doctrine\Common\Annotations\AnnotationRegistry::registerLoader(array($composer_autoloader, 'loadClass'));