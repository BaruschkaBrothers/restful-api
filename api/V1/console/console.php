<?php

// Execute bootstrap
$app = require __DIR__ . '/../bootstrap/app.php';

// Execute app on successful bootstrap
if(isset($app)) {
	$app['console']->run();
} else {
	//Show 404 Error if Silex has not been set up
	header("HTTP/".HTTP_VERSION." 404 Not Found");
	echo "<h1>404 Not Found</h1>";
	echo "The page that you have requested could not be found.";
	exit();
}