<?php

// Execute bootstrap
$app = require __DIR__ . '/../bootstrap/app.php';

// Execute app on successful bootstrap
if(isset($app)) {
	$request = OAuth2\HttpFoundationBridge\Request::createFromGlobals();
	$app->run($request); //Analyze request and return result!
} else {
	//Show 404 Error if Silex has not been set up
	header("HTTP/".HTTP_VERSION." 404 Not Found");
	echo "<h1>404 Not Found</h1>";
	echo "The page that you have requested could not be found.";
	exit();
}