<?php

namespace BaruschkaBrothers\RestAPI\Auth;

use BaruschkaBrothers\RestAPI\Config\Config;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Encoder\BasePasswordEncoder;

class PasswordEncoder extends BasePasswordEncoder {
	
	/**
	 * {@inheritDoc}
	 * @see \Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface::encodePassword()
	 */
	public function encodePassword($raw, $salt) {
		if ($this->isPasswordTooLong($raw)) {
			throw new BadCredentialsException('Invalid password.');
		}
	
		$hash = password_hash($raw, Config::DEFAULT_HASHING_ALGORITHM);
	
		return $hash ? $hash : NULL;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface::isPasswordValid()
	 */
	public function isPasswordValid($encoded, $raw, $salt) {
		if ($this->isPasswordTooLong($raw)) {
			return false;
		}
	
		return password_verify($raw, $encoded);
	}
	
}