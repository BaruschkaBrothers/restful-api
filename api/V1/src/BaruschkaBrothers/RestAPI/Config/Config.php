<?php namespace BaruschkaBrothers\RestAPI\Config;

abstract class Config {
	
	/**
	 * The current version of the development branch.
	 * 
	 * @var string
	 */
	const BUILD_VERSION = '0.1';
	
	/**
	 * The stage of the product lifecycle the project is currently in.
	 * 
	 * @var EnvironmentType
	 */
	const ENVIRONMENT_TYPE = EnvironmentType::DEVELOPMENT;
	
	/**
	 * States if debug mode is enabled or not.
	 * 
	 * @var boolean
	 */
	const DEBUG_MODE = Config::ENVIRONMENT_TYPE == EnvironmentType::DEVELOPMENT || Config::ENVIRONMENT_TYPE == EnvironmentType::TEST;
	
	/**
	 * The timezone to use when working with date times.
	 * 
	 * @var string
	 */
	const SERVER_TIMEZONE = 'Europe/London';
	
	/**
	 * The format to use when working with date times.
	 * 
	 * @var string
	 * @see Config::DATABASE_DRIVER
	 */
	const DATE_TIME_FORMAT = 'Y-m-d H:i:s'; // MySQL Datetime format
	
	/**
	 * The algorithm to use when hashing passwords.
	 * 
	 * @var mixed
	 */
	const DEFAULT_HASHING_ALGORITHM = PASSWORD_BCRYPT;
	
	/**
	 * The driver type to connect to the database.
	 * 
	 * @var string
	 */
	const DATABASE_DRIVER = 'pdo_mysql';
	
	/**
	 * The host url of the database.
	 * 
	 * @var string
	 * @see Config::DATABASE_DRIVER
	 */
	const DATABASE_HOST = 'localhost';
	
	/**
	 * The name of the database to connect to on the given host.
	 * 
	 * @var string
	 * @see Config::DATABASE_HOST
	 */
	const DATABASE_NAME = 'bb_rest_01';
	
	/**
	 * The name of the user to use to connect to the given host.
	 * 
	 * @var string
	 * @see Config::DATABASE_HOST
	 */
	const DATABASE_USER = 'root';
	
	/**
	 * The password for the defined user.
	 * 
	 * @var string
	 * @see Config::DATABASE_USER
	 */
	const DATABASE_PASSWORD = '1337';
	
}