<?php namespace BaruschkaBrothers\RestAPI\Config;

abstract class EnvironmentType {
	
	const DEVELOPMENT = 0;
	const TEST = 1;
	const PRODUCTION = 2;
	
}