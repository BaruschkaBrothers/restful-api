<?php namespace BaruschkaBrothers\RestAPI\Core;

use BaruschkaBrothers\RestAPI\Auth\PasswordEncoder;
use BaruschkaBrothers\RestAPI\Config\Config;
use BaruschkaBrothers\RestAPI\Config\EnvironmentType;
use BaruschkaBrothers\RestAPI\RouteController\AuthRoutesController;
use Basster\Silex\Provider\Swagger\SwaggerProvider;
use Dflydev\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use Kurl\Silex\Provider\DoctrineMigrationsProvider;
use OAuth2\GrantType\AuthorizationCode;
use OAuth2\GrantType\ClientCredentials;
use OAuth2\GrantType\RefreshToken;
use OAuth2\GrantType\UserCredentials;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ValidatorServiceProvider;

class Application extends \Silex\Application {
	
	// === Constructor ===
	
	/**
	 * 
	 * @param EnvironmentType $environmentType
	 * @param bool $debugMode
	 * @see Config
	 */
	public function __construct($environmentType, $debugMode) {
		
		parent::__construct();
		
		///////////////////////
		// 	 Setup Config	 //
		///////////////////////
		
		// Basic Configuration
		$this['config.environmentType'] = $environmentType;
		
		// Define Debug State
		$this['debug'] = $debugMode;
		
		// Define DB options
		$this['db.options'] = array
		(
			'driver' => Config::DATABASE_DRIVER,
			'host' => Config::DATABASE_HOST,
			'dbname' => Config::DATABASE_NAME,
			'user' => Config::DATABASE_USER,
			'password' => Config::DATABASE_PASSWORD
		);
		
		// Define twig options
		$this['twig.path'] = __DIR__ . '/../../../../assets/views';
		
		///////////////////////
		// Register services //
		///////////////////////
		
		// Password Encoder
		$this['security.password_encoder'] = new PasswordEncoder();
		
		// Monolog Logger
		$this->register(new MonologServiceProvider(),
			array(
				'monolog.logfile' => __DIR__ . '/../../../../logs/app.log'
			)
		);
		
		// Validator
		$this->register(new ValidatorServiceProvider());
		
		// Twig
		$this->register(new TwigServiceProvider());
		
		// Doctrine
		$this->register(new DoctrineServiceProvider());
		$this->register(new DoctrineOrmServiceProvider(), [
			'orm.em.options' => array(
				'mappings' => array(
						array(
							'type' => 'annotation',
							'namespace' => 'BaruschkaBrothers\RestAPI\Data\Entity',
							'path' => __DIR__.'/../Data/Entity',
							'use_simple_annotation_reader' => FALSE // Default: TRUE. FALSE enables namespacing in annotations
						)
				),
				'auto_generate_proxies' => TRUE
			)
		]);
		
		// Swagger-php
		
		/**
		 * @SWG\Swagger(
		 * 		schemes={"http"},
		 *  	host=SERVER_HOST,
		 *  	basePath="/",
		 *      @SWG\Info(title="Baruschka Brothers - RESTful Web API",
		 *		 			 version=BaruschkaBrothers\RestAPI\Config\Config::BUILD_VERSION)
		 * )
		 * 
		 */
		$this->register(new SwaggerProvider(), [
				'swagger.servicePath' => __DIR__ . '/..',
				'swagger.apiDocPath' => '/api-docs'
		]);
		
		// Swagger-UI
		
		// OAuth2 Server
		$clientStorage = $this['orm.em']->getRepository('BaruschkaBrothers\RestAPI\Data\Entity\OAuthClient');
		$userStorage = $this['orm.em']->getRepository('BaruschkaBrothers\RestAPI\Data\Entity\OAuthUser');
		$tokenStorage = $this['orm.em']->getRepository('BaruschkaBrothers\RestAPI\Data\Entity\OAuthAccessToken');
		$authorizationCodeStorage = $this['orm.em']->getRepository('BaruschkaBrothers\RestAPI\Data\Entity\OAuthAuthorizationCode');
		
		// TODO: Inline Configuration does not work for this service for some unknown reason... 
		$this['oauth2.storage'] = [
			'client_credentials' => $clientStorage,
			'user_credentials'   => $userStorage,
			'access_token'       => $tokenStorage,
			'authorization_code' => $authorizationCodeStorage,
			'refresh_token' => $tokenStorage
		];
		
		$this['oauth2.config'] = [];
		
		$this['oauth2.granttypes'] =
		[
			'authorization_code' => new AuthorizationCode($authorizationCodeStorage),
			'user_credentials'   => new UserCredentials($userStorage),
			'client_credentials' => new ClientCredentials($clientStorage),
			'refresh_token'      => new RefreshToken($tokenStorage, array (
					'always_issue_new_refresh_token' => true
			))
		];
		
		$this->register(new \BaruschkaBrothers\RestAPI\ServiceProvider\OAuth2ServerProvider());
		
		// Doctrine Migrations
		$this['console'] = new \Symfony\Component\Console\Application();
		
		$this->register(
			new DoctrineMigrationsProvider(),
			array(
				'migrations.directory' => __DIR__ . '/../Data/Migration',
				'migrations.namespace' => 'BaruschkaBrothers\\RestAPI\\Data\\Migration',
			)
		);
		
		
		// Register routes
		$this->mount("/", new AuthRoutesController());
		
		$this->boot();
	}
	
	// === Magic Methods ===
	
	/**
	 * Executes the silex application.
	 */
	public function __invoke() {
		$this->run();
	}
	
}