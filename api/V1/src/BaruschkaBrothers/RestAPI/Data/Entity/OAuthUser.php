<?php namespace BaruschkaBrothers\RestAPI\Data\Entity;

use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Doctrine\ORM\Mapping AS ORM;
use Swagger\Annotations as SWG;

/**
 * OAuthUser
 * 
 * @ORM\Entity(repositoryClass="BaruschkaBrothers\RestAPI\Data\Repository\OAuthUserRepository")
 * @SWG\Definition()
 */
class OAuthUser implements AdvancedUserInterface
{
	
	// === Fields ===
	
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @var integer
	 * @SWG\Property()
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", unique=true)
	 * @var string
	 */
	private $email;
	
	/**
	 * @ORM\Column(type="string", unique=true, nullable=false)
	 * @var string
	 */
	private $username;

	/**
	 * @ORM\Column(type="string", nullable=false)
	 * @var string
	 */
	private $password;
	
	/**
	 * @ORM\Column(type="boolean")
	 * @var boolean
	 */
	private $enabled;
	
	/**
	 * @ORM\Column(type="datetime")
	 * @var \DateTime
	 */
	private $expiresAt;
	
	// === Magic Methods ===
	
	public function toArray()
	{
		return [
			'user_id' => $this->id,
			'username' => $this->username,
			'enabled' => $this->enabled
		];
	}
	
	// === AdvancedUserInterface Impl ===
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Symfony\Component\Security\Core\User\UserInterface::getUsername()
	 */
	public function getUsername() {
		return $this->username;
	}
	
	/**
	 * Get password
	 *
	 * @return string
	 */
	public function getPassword()
	{
		return $this->password;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Symfony\Component\Security\Core\User\AdvancedUserInterface::isAccountNonExpired()
	 */
	public function isAccountNonExpired() {
		$currentDateTime = date(\BaruschkaBrothers\RestAPI\Config\Config::DATE_TIME_FORMAT, time());
		return $this->getExpiresAt() > $currentDateTime;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Symfony\Component\Security\Core\User\AdvancedUserInterface::isAccountNonLocked()
	 */
	public function isAccountNonLocked() {
		return true; // TODO: Account locking is not supported yet
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * @see \Symfony\Component\Security\Core\User\AdvancedUserInterface::isCredentialsNonExpired()
	 */
	public function isCredentialsNonExpired() {
		return true; // TODO: Credentials can not expire yet
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * @see \Symfony\Component\Security\Core\User\AdvancedUserInterface::isEnabled()
	 */
	public function isEnabled() {
		return $this->enabled;
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Symfony\Component\Security\Core\User\UserInterface::getSalt()
	 */
	public function getSalt() {
		return; // TODO: Salt is not supported yet
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Symfony\Component\Security\Core\User\UserInterface::getRoles()
	 */
	public function getRoles() {
		return; // TODO: Roles are not supported yet
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \Symfony\Component\Security\Core\User\UserInterface::eraseCredentials()
	 */
	public function eraseCredentials() {
		// TODO: Check implementation details of this method online
	}
	
	// === Other Getters & Setters ===

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set email
	 *
	 * @param string $email
	 * @return User
	 */
	public function setEmail($email)
	{
		$this->email = $email;
		return $this;
	}

	/**
	 * Get email
	 *
	 * @return string
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * Set password
	 *
	 * @param string $password
	 * @return User
	 */
	public function setPassword($password)
	{
		$this->password = $this->encryptField($password);
		return $this;
	}
	
	/**
	 *
	 * @return the \DateTime
	 */
	public function getExpiresAt() {
		return $this->expiresAt;
	}
	
	/**
	 *
	 * @param \DateTime $expiresAt        	
	 */
	public function setExpiresAt(\DateTime $expiresAt) {
		$this->expiresAt = $expiresAt;
		return $this;
	}
	

}