<?php

namespace BaruschkaBrothers\RestAPI\Data\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use BaruschkaBrothers\RestAPI\Auth\PasswordEncoder;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161214180628 extends AbstractMigration
{
	/**
	 * {@inheritDoc}
	 * @see \Doctrine\DBAL\Migrations\AbstractMigration::postUp()
	 */
	public function postUp($schema) {
		$encoder = new PasswordEncoder();
		
		// Create admin account
		$this->connection->createQueryBuilder()
		->insert('OAuthUser')
		->values([
			'username' => '?',
			'password' => '?',
			'enabled' => 1
		])
		->setParameter(0, 'admin')
		->setParameter(1, $encoder->encodePassword('1337', NULL))
		->execute();
		
		parent::postUp($schema);
	}
	
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE OAuthClient (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE OAuthUser (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, enabled TINYINT(1) NOT NULL, expiresAt DATETIME NOT NULL, UNIQUE INDEX UNIQ_895130B7E7927C74 (email), UNIQUE INDEX UNIQ_895130B7F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE OAuthAccessToken (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE OAuthAuthorizationCode (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE OAuthClient');
        $this->addSql('DROP TABLE OAuthUser');
        $this->addSql('DROP TABLE OAuthAccessToken');
        $this->addSql('DROP TABLE OAuthAuthorizationCode');
    }
}
