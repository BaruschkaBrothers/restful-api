<?php namespace BaruschkaBrothers\RestAPI\Data\Repository;

use Doctrine\ORM\EntityRepository;
use BaruschkaBrothers\RestAPI\Data\Entity\OAuthAccessToken;
use OAuth2\Storage\AccessTokenInterface;
use OAuth2\Storage\RefreshTokenInterface;

class OAuthAccessTokenRepository extends EntityRepository implements AccessTokenInterface, RefreshTokenInterface
{
	public function getAccessToken($oauthToken)
	{
		$token = $this->findOneBy(['token' => $oauthToken]);
		if ($token) {
			$token = $token->toArray();
			$token['expires'] = $token['expires']->getTimestamp();
		}
		return $token;
	}

	public function setAccessToken($oauthToken, $clientIdentifier, $user_id, $expires, $scope = null)
	{
		$client = $this->_em->getRepository('BaruschkaBrothers\RestAPI\Data\Entity\OAuthClient')
		->findOneBy(['client_identifier' => $clientIdentifier]);
		$user = $this->_em->getRepository('BaruschkaBrothers\RestAPI\Data\Entity\OAuthUser')
		->findOneBy(['email' => $user_id]);
		$token = OAuthAccessToken::fromArray([
				'token'     => $oauthToken,
				'client'    => $client,
				'user'      => $user,
				'expires'   => (new \DateTime())->setTimestamp($expires),
				'scope'     => $scope,
		]);
		$this->_em->persist($token);
		$this->_em->flush();
	}
	/**
	 * {@inheritDoc}
	 * @see \OAuth2\Storage\RefreshTokenInterface::getRefreshToken()
	 */
	public function getRefreshToken($refresh_token) {
		return $this->getAccessToken($refresh_token);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \OAuth2\Storage\RefreshTokenInterface::setRefreshToken()
	 */
	public function setRefreshToken($refresh_token, $client_id, $user_id, $expires, $scope = null) {
		$this->setAccessToken($refresh_token, $client_id, $user_id, $expires, $scope);
	}

	/**
	 * {@inheritDoc}
	 * @see \OAuth2\Storage\RefreshTokenInterface::unsetRefreshToken()
	 */
	public function unsetRefreshToken($refresh_token) {
		if ($refresh_token) {
			$this->_em->remove($refresh_token);
			$this->_em->flush();
		}
	}

}