<?php namespace BaruschkaBrothers\RestAPI\Data\Repository;

use Doctrine\ORM\EntityRepository;
use OAuth2\Storage\ClientCredentialsInterface;
use BaruschkaBrothers\RestAPI\Auth\PasswordEncoder;

class OAuthClientRepository extends EntityRepository implements ClientCredentialsInterface
{
	public function getClientDetails($clientIdentifier)
	{
		$client = $client = $this->findOneBy(['client_identifier' => $clientIdentifier]);
		if ($client) {
			return $client->toArray();
		} else return FALSE;
	}

	public function checkClientCredentials($clientIdentifier, $clientSecret = NULL)
	{
		$client = $client = $this->findOneBy(['client_identifier' => $clientIdentifier]);
		if ($client) {
			$encoder = new PasswordEncoder();
			return $encoder->isPasswordValid($client->getClientSecret(), $clientSecret, NULL);
		}
		return false;
	}

	public function checkRestrictedGrantType($clientIdentifier, $grantType)
	{
		// TODO: implement grant-type restriction
		return true;
	}

	public function isPublicClient($clientIdentifier)
	{
		return true;
	}

	public function getClientScope($clientIdentifier)
	{
		return null;
	}
}