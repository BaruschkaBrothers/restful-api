<?php namespace BaruschkaBrothers\RestAPI\Data\Repository;

use Doctrine\ORM\EntityRepository;
use OAuth2\Storage\UserCredentialsInterface;
use BaruschkaBrothers\RestAPI\Auth\PasswordEncoder;

class OAuthUserRepository extends EntityRepository implements UserCredentialsInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \OAuth2\Storage\UserCredentialsInterface::checkUserCredentials()
	 */
	public function checkUserCredentials($username, $password)
	{
		$user = $this->findOneBy(['username' => $username]);
		if ($user) {
			$encoder = new PasswordEncoder();
			return $encoder->isPasswordValid($user->getPassword(), $password, NULL);
		}
		
		return false;
	}

	/**
	 * {@inheritDoc}
	 * @see \OAuth2\Storage\UserCredentialsInterface::getUserDetails()
	 */
	public function getUserDetails($username)
	{
		$user = $this->findOneBy(['username' => $username]);
		if ($user) {
			return $user->toArray();
		} else return FALSE;
	}
}