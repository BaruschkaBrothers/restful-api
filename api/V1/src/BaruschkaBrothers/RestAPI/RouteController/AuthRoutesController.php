<?php

namespace BaruschkaBrothers\RestAPI\RouteController;

use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;

/**
 * OAuth2 Authorization route.
 * 
 * @author Konstantin Schaper
 * @see https://tools.ietf.org/html/rfc6749#page-22
 * @SWG\Tag (
 * 		name="Authentication",
 * 		description="Contains all OAuth2 authentication related resources."
 * )
 */
class AuthRoutesController implements ControllerProviderInterface {
	
	/**
	 * {@inheritDoc}
	 * @see \Silex\Api\ControllerProviderInterface::connect()
	 */
	public function connect(Application $app) {
		$controllers = $app['controllers_factory'];
		
		$controllers->get('/authorize', array(new self(), 'authorize_get'))->bind('oauth2_authorize');
		$controllers->post('/token', array(new self(), 'token_post'))->bind('oauth2_token');
		
		return $controllers;
	}
	
	/**
	 * @param Request $request
	 * @param Application $app
	 * 
	 * @SWG\Get(
	 * 	path="/authorize",
	 * 	summary="Main ",
	 * 	tags={"Authentication"},
	 * 	@SWG\Response(
	 * 		response=200,
	 * 		description="The authenticated access token for the given credentials."
	 * 	)
	 * )
	 */
	public function authorize_get(Request $request, Application $app) {
		
	}
	
	/**
	 * @param Request $request
	 * @param Application $app
	 * @return unknown
	 * 
	 * @SWG\Post(
	 * 	path="/authorize",
	 * 	summary="Main ",
	 * 	tags={"Authentication"},
	 * 	@SWG\Response(
	 * 		response=200,
	 * 		description="The authentication has been successful and a token is returned."
	 * 	)
	 * )
	 */
	public function authorize_post(Request $request, Application $app) {
		
		$server = $app['oauth2.server'];
		
		$response = $app['oauth2.response'];
		
		// check the form data to see if the user authorized the request
		$authorized = (bool) $app['request']->request->get('authorize');
		
		// call the oauth server and return the response
		return $server->handleAuthorizeRequest($app['request'], $response, $authorized);
	}
	
	/**
	 * 
	 * @param Request $request
	 * @param Application $app
	 * @return unknown
	 * 
	 * @SWG\Get(
	 * 		path="/token",
	 * 		summary="Retrieve Access or Refresh Token.",
	 * 		tags={"Authentication"},
	 * 		@SWG\Response(
	 * 			response=200,
	 * 			description="The authentication has been successful and a token is returned."
	 * 		)
	 * )
	 */
	public function token_post(Request $request, Application $app) {
		
		$server = $app['oauth2.server'];
		$response = $app['oauth2.response'];
		
		// let the oauth2-server-php library do all the work!
		return $server->handleTokenRequest($request, $response);
	}
		
}