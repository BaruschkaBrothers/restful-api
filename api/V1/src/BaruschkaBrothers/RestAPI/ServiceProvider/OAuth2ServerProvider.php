<?php namespace BaruschkaBrothers\RestAPI\ServiceProvider;

use Pimple\ServiceProviderInterface;
use Pimple\Container;
use OAuth2\Server;
use OAuth2\HttpFoundationBridge\Response;

class OAuth2ServerProvider implements ServiceProviderInterface {
	
	public function register(Container $app) {
		$server = new Server($app['oauth2.storage'], $app['oauth2.config'], $app['oauth2.granttypes']);
		
		$app['oauth2.server'] = $server;
		$app['oauth2.response'] = new Response();
	}
	
}