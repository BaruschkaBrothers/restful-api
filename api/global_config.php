<?php namespace App;

/* ============================================
 * ===										===
 * ===		@version 1.0.0					===
 * ===		@author Konstantin Schaper		===
 * ===										===
 * ============================================
 */

/**
 * 	Defines the current major version.
 * 	Used for automatic redirects.
 */
define("API_VERSION", 'V1');

/**
 * Defines the server url as global variable for inline use.
 */
define("SERVER_HOST", $_SERVER['HTTP_HOST']);

?>